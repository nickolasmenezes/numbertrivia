import 'dart:ui';

import 'package:clean_architecture_project/features/number_trivia/domain/entities/number_trivia.dart';
import 'package:clean_architecture_project/features/number_trivia/presentation/bloc/number_trivia_bloc.dart';
import 'package:clean_architecture_project/features/number_trivia/presentation/widgets/widgets.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../injection_container.dart';

class NumberTriviaPage extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
   return Scaffold(
     appBar: AppBar(
       title: Text('Number Trivia'),
     ),
     body: buildBody(context),
   );
  }

  BlocProvider<NumberTriviaBloc> buildBody(BuildContext context) {
    return BlocProvider(
     create:  (_) => sl<NumberTriviaBloc>(),
     child: Center(
       child: Padding(
         padding: const EdgeInsets.all(10),
         child: Column(
           children: <Widget>[
             SizedBox(height: 10),
             BlocBuilder<NumberTriviaBloc, NumberTriviaState>(
                builder: (context, state) {  
                  if(state is Empty){
                    return MessageDisplay(message: 'Start searching',);
                  }else if(state is Loading){
                    return LoadingWidget();
                  }else if(state is Loaded){
                    return TriviaDisplay(numberTrivia: state.trivia);
                  }else if(state is Error){
                    return MessageDisplay(message: state.message);
                  }else{
                    return MessageDisplay(message: 'Start searching',);
                  }
                },
             ),
             SizedBox(height: 20),
             TriviaControls(),
           ],
         ),
       ),
     ),
   );
  }
}

class TriviaControls extends StatefulWidget {
  const TriviaControls({
    Key key,
  }) : super(key: key);

  @override
  _TriviaControlsState createState() => _TriviaControlsState();
}

class _TriviaControlsState extends State<TriviaControls> {
  final controller = TextEditingController();
  String inputString;

  @override
  Widget build(BuildContext context) {
    return Column(
      children:  <Widget>[ 
        TextField(
          controller: controller,
          keyboardType: TextInputType.number,
          decoration: InputDecoration(
            border:  OutlineInputBorder(),
            hintText: 'Input a number',
          ),  
          onChanged: (value){
            inputString = value;
          },),
        SizedBox(height: 10),
        Row(children: <Widget>[
         Expanded(
           child: ElevatedButton(
             child: Text('Search'),
             style: ElevatedButton.styleFrom(primary: Theme.of(context).accentColor), 
             onPressed: () { dispatchConcrete(); },
             ),
           ),
         SizedBox(width: 10),
         Expanded(
          child: ElevatedButton(
             child: Text('Get Random trivia'),
             onPressed: () { dispatchRandom(); },
             ),
           ),
        ],
        ),
      ], 
    );
  }

  void dispatchConcrete(){
    controller.clear();
    BlocProvider.of<NumberTriviaBloc>(context).add(GetTriviaForConcreteNumber(inputString));
  }

   void dispatchRandom(){
    controller.clear();
    BlocProvider.of<NumberTriviaBloc>(context).add(GetTriviaForRandomNumber());
  }
}
