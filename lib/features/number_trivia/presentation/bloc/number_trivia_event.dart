part of 'number_trivia_bloc.dart';

@immutable
abstract class NumberTriviaEvent extends Equatable {
  NumberTriviaEvent([List props = const<dynamic> []]);
}

class GetTriviaForConcreteNumber extends NumberTriviaEvent{
  final String numberString;

  GetTriviaForConcreteNumber(this.numberString) : super([numberString]);

  @override
  List<Object> get props => [GetTriviaForConcreteNumber];
}

class GetTriviaForRandomNumber extends NumberTriviaEvent{
  @override
  List<Object> get props => [GetTriviaForRandomNumber];
}