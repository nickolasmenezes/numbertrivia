import 'package:clean_architecture_project/core/error/failures.dart';
import 'package:dartz/dartz.dart';

class InputConverter {
  Either<Failure, int> stringToUnsignedInteger(String str) {
    try{
      int integer = int.parse(str);
      if(integer < 0)  throw FormatException();
      return Right(integer);
    }on FormatException{
      return Left(InvalidInputFailure());
    }  
  }
}

class InvalidInputFailure extends Failure {
  @override
  List<Object> get props => [InvalidInputFailure];
}