import 'package:clean_architecture_project/core/network/network_info.dart';
import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

class MockDataConnectionChecker extends Mock implements DataConnectionChecker{}

void main(){
  NetworkInfo networkInfoImpl;
  MockDataConnectionChecker mockDataConnectionChecker;

  setUp((){
    mockDataConnectionChecker = MockDataConnectionChecker();
    networkInfoImpl = NetworkInfoImpl(mockDataConnectionChecker);
  });

  group('isConnected', (){
    test('should forward the call to DataConnectionChecker.hasConnection', 
    () async { 
       final tHasConncetionFuture = Future.value(true);

      when(mockDataConnectionChecker.hasConnection).thenAnswer((_) => tHasConncetionFuture);
       
       final result = networkInfoImpl.isConnected;

       verify(mockDataConnectionChecker.hasConnection);
       expect(result, tHasConncetionFuture);
    });
  });
  

}