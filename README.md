# clean_architecture_project

This project is a Flutter TDD Clean Architecture Flutter application.

It's organized:

- Entities & Use Cases
- Domain Layer Refactoring
- Data Layer Overview & Models
- Contracts of Data Sources
- Repository Implementation
- Network Info
- Local Data Source
- Remote Data Source
- Bloc Scaffolding & Input Conversion
- Bloc Implementation 1/2 (and Basics of Test Driven Development)
- Bloc Implementation 2/2
- Dependency Injection
- User Interface 


From ResoCoder (https://resocoder.com/)


A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
